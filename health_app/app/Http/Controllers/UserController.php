<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserDetails;

class UserController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */


    public function login(Request $request)
    {
    	$response_data = array();

    	$response_data['status'] = 401;
    	$response_data['msg'] = 200;	
    	$response_data['data'] = array();

        $email = $request->input('email');
        $password = $request->input('password');
        $user_type = $request->input('user_type');

        $user_data = User::where('email',$email)->where('user_type',$user_type)->where('password',md5( $password) )->first();

        if(!empty($user_data)) {
            $response_data['status'] = 200;
        	$user_data->api_token = $this->encrypt();
        	$user_data->save();
        }

        $response_data['data'] = $user_data;
        
        return response()->json($response_data);
        //
    }


    public function createUser(Request $request) {

    	$response_data = array();

    	$response_data['status'] = 400;
    	$response_data['msg'] = 'Added sucessfully';	
    	$response_data['data'] = array();

    	$user_data = User::where('email',$request->email)->first();
    	if(empty($user_data)) {

    		$insert_data = array();
    		$insert_data['email'] = $request->email;
    		$insert_data['first_name'] = $request->first_name;
    		$insert_data['last_name'] = $request->last_name;
    		$insert_data['gender'] = $request->gender;
            $insert_data['mobile'] = $request->mobile;
            $insert_data['user_type'] = $request->emp_type;
            $insert_data['dob'] = date('Y-m-d H:i:s', strtotime($request->dob));
    		$insert_data['address1'] = $request->address1;
            $insert_data['address2'] = $request->address2;
            $insert_data['city'] = $request->city;
            $insert_data['states'] = $request->states;
            $insert_data['zip'] = $request->zip;
            $insert_data['emp_type'] = $request->emp_type;
            $insert_data['education'] = $request->education;
            $insert_data['speciality'] = $request->speciality;
            $insert_data['password'] =   md5('123456');
            $insert_data['created_at'] = date('Y-m-d H:i:s');

            if(isset($request->weight)) {
                $insert_data['weight'] = $request->weight;
                $insert_data['height'] = $request->height;
            }

    		User::insert($insert_data);
            $response_data['status'] = 200;

    	}else {
            $update_data = User::find($user_data->id);

            $update_data->email = $request->email;
            $update_data->first_name = $request->first_name;
            $update_data->last_name = $request->last_name;
            $update_data->gender = $request->gender;
            $update_data->mobile = $request->mobile;
            $update_data->user_type = $request->emp_type;
            $update_data->dob = date('Y-m-d H:i:s', strtotime($request->dob));
            $update_data->address1 = $request->address1;
            $update_data->address2 = $request->address2;
            $update_data->city = $request->city;
            $update_data->states = $request->states;
            $update_data->zip = $request->zip;
            $update_data->emp_type = $request->emp_type;
            $update_data->education = $request->education;
            $update_data->speciality = $request->speciality;

            if(isset($request->weight)) {
                $update_data->weight = $request->weight;
                $update_data->height = $request->height;
            }

            $update_data->save();
            $response_data['msg'] = 'Updated sucessfully'; 
            $response_data['status'] = 200;   

    	}
    	return response()->json($response_data);
    }

    public function listUsers(Request $request) {
       $response_data = array();
       $user_type = $request->listType;
       if($user_type == 'users') {
         $user_data = User::where('user_type','!=','Patient')->get()->toArray();
       }else {
         $user_data = User::where('user_type','=','Patient')->get()->toArray();
       }
       $response_data['data'] = $user_data;
       return response()->json($response_data);

    }


    public function getUser($user_id) {
       
       $user_data = User::find($user_id)->toArray();
       return response()->json($user_data);

    }


    public function addPatientDetails(Request $request) {

        $response_data = array();

        $response_data['status'] = 200;
        $response_data['msg'] = 'Added sucessfully';    
        $response_data['data'] = array();

        $insert_data = array();
        $insert_data['email'] = $request->email;
        $insert_data['first_name'] = $request->first_name;
        $insert_data['last_name'] = $request->last_name;
        $insert_data['gender'] = $request->gender;
        $insert_data['mobile'] = $request->mobile;
        $insert_data['user_type'] = $request->emp_type;
        $insert_data['dob'] = date('Y-m-d H:i:s', strtotime($request->dob));
        $insert_data['address1'] = $request->address1;
        $insert_data['address2'] = $request->address2;
        $insert_data['city'] = $request->city;
        $insert_data['states'] = $request->states;
        $insert_data['zip'] = $request->zip;
        $insert_data['emp_type'] = $request->emp_type;
        $insert_data['education'] = $request->education;
        $insert_data['speciality'] = $request->speciality;
        $insert_data['password'] = rand(100000, 999999);
        $insert_data['created_at'] = date('Y-m-d H:i:s');
        User::insert($insert_data);
        
        
        return response()->json($response_data);
    }


    private function encrypt($input = '') {
    	$milliseconds = round(microtime(true) * 1000);
    	return password_hash($milliseconds, PASSWORD_DEFAULT);
	} 


    public function addPatientMedicalDetails(Request $request) { 

            $response_data = array();

            $response_data['status'] = 200;
            $response_data['msg'] = 'Added sucessfully';    
            $response_data['data'] = array();

            $insert_data = array();
            $insert_data['user_id'] = $request->user_id;
            $insert_data['symptoms'] = $request->symptoms;
            $insert_data['diagnosis'] = $request->diagnosis;
            $insert_data['symptoms'] = $request->symptoms;
            $insert_data['pres_medicine'] = $request->pres_medicine;
            $insert_data['test_order'] = $request->test_order;
            $insert_data['sugg_specialist'] = $request->sugg_specialist;
            $insert_data['created_at'] = date('Y-m-d H:i:s');
            UserDetails::insert($insert_data);
            return response()->json($response_data);
    }

    public function getSpeciality() {
            $response_data = array();

            $response_data['status'] = 200;
            $response_data['msg'] = 'sucessfully';    
            $response_data['data'] = array();

            $result_array  = \DB::select('select * from specialist_master');

            //print_r($result_array);

            return response()->json($result_array);

    }

    public function getPatientMedicalDetails(Request $request) {
            $response_data = array();

            $response_data['status'] = 200;
            $response_data['msg'] = 'sucessfully';    
            $response_data['data'] = array();

            $result_array  = \DB::select('select * from user_details where user_id = ?',[$request->user_id]);

            //print_r($result_array);

            return response()->json($result_array);

    }

}