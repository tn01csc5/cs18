<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDetails extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */


    protected $table = 'user_details';

    protected $dates = ['created_at'];
}