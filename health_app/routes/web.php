<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () {
// 	echo 'Welcome';        
// });

$router->post('user/login', 'UserController@login');


$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('/', function () {
        // Uses Auth Middleware
        echo 'Welcome';  
    });

    $router->post('user/list', 'UserController@listUsers');
    $router->post('user/add', 'UserController@createUser');
    $router->post('user/edit/{user_id}', 'UserController@getUser');
    $router->post('user/add_patient_details', 'UserController@addPatientDetails');
    $router->post('user/add_patient_medical_info', 'UserController@addPatientMedicalDetails');
    $router->post('user/get_patient_medical_info', 'UserController@getPatientMedicalDetails');
    $router->post('speciality', 'UserController@getSpeciality');
});

