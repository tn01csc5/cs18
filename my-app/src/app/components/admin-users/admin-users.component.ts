import { AfterViewInit, Component, OnInit, Renderer } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.less']
})
export class AdminUsersComponent implements AfterViewInit, OnInit {

  dtOptions: DataTables.Settings = {};

  constructor(private renderer: Renderer, private router: Router) { }

  ngOnInit(): void {
    this.dtOptions = {
      ajax: { 
        url: environment.api_base_url + 'user/list',
        method: 'POST',
        data : {
          'listType' : 'users'
        },
        headers : {
          'Api-Token' : sessionStorage.getItem('token')
        }
      },
      columns: [{
        title: 'ID',
        data: 'id'
      }, {
        title: 'First name',
        data: 'first_name'
      }, {
        title: 'Last name',
        data: 'last_name'
      }, {
        title: 'Mobile',
        data: 'mobile'
      }, {
        title: 'Type',
        data: 'user_type'
      }, {
        title: 'Action',
        render: function (data: any, type: any, full: any) {
          return '<button class="waves-effect btn" view-person-id="'+full.id+'">View</button>';
        }
      }],

    };
  }

  ngAfterViewInit(): void {
    this.renderer.listenGlobal('document', 'click', (event) => {
      if (event.target.hasAttribute("view-person-id")) {
        this.router.navigate(["/user_reg/edit/" + event.target.getAttribute("view-person-id")]);
      }
    });
  }
}