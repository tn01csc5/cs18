import { Component, OnInit } from '@angular/core';
import { UserServiceService } from "../../services/user-service.service";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  constructor(private _userServ: UserServiceService, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
  }

  errorMsg: any;
  responceData: any;

  onSubmit(values) { 
  	
  	this._userServ.login(values).subscribe(resdata => this.responceData = resdata, reserror => this.errorMsg = reserror, () => {
  		
  		if(this.responceData.status == '200') {

	  		sessionStorage.setItem('token', this.responceData.data.api_token);
        sessionStorage.setItem('user_data', JSON.stringify(this.responceData.data));

	  		this.toastr.success('Login Successfully', 'Success');
	  		this.router.navigate(['dashboard']);
  		}else {
  			this.toastr.error('Invalid Username Or Password', 'Failed');
  		}
    });
  }

}
