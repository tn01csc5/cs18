import { Component, OnInit } from '@angular/core';
import { UserServiceService } from "../../services/user-service.service";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {

  constructor(private _userServ: UserServiceService, private router: Router, private toastr: ToastrService) { }

  patientHistory:any;
    errorMsg: any;
  responceData: any;
  submitted = false;

  ngOnInit() {
  	this.responceData = JSON.parse(sessionStorage.getItem('user_data'));

    let user_id = this.responceData.id;

    this._userServ.getPatientHistory(user_id).subscribe(resdata => this.patientHistory = resdata, reserror => this.errorMsg = reserror, () => {
          });

  }
  


}
