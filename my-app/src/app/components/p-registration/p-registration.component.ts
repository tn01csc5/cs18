import { Component, OnInit } from '@angular/core';
import { UserServiceService } from "../../services/user-service.service";
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-p-registration',
  templateUrl: './p-registration.component.html',
  styleUrls: ['./p-registration.component.less']
})
export class PRegistrationComponent implements OnInit {

	constructor(private _userServ: UserServiceService, private router: Router, private toastr: ToastrService, private route: ActivatedRoute) { }

	regFields = {
		id : 0,
		first_name : '',
		last_name : '',
		gender : 'Male',
		email : '',
		dob : '',
		contact_number : '',
		address1 : '',
		address2 : '',
		city : '',
		states: 'Maharashtra',
		zip : '',
		emp_type : 'Patient',
		education : '',
		speciality : '',
		weight: '',
		height : ''
	}

	errorMsg: any;
	responceData: any;
	submitted = false;
	pType = 'Add New Patient';

	ngOnInit() {

		this.route.params.subscribe(params => {
			this.regFields.id = parseInt(params['id']);
			console.log(params);
			if(this.regFields.id > 0) {
				this._userServ.edit(this.regFields.id).subscribe(resdata => this.responceData = resdata, reserror => this.errorMsg = reserror, () => {
					this.regFields = {
							id : this.responceData.id,
							first_name : this.responceData.first_name,
							last_name : this.responceData.last_name,
							gender : this.responceData.gender,
							email : this.responceData.email,
							dob : this.responceData.dob,
							contact_number : this.responceData.mobile,
							address1 : this.responceData.address1,
							address2 : this.responceData.address2,
							city : this.responceData.city,
							states: this.responceData.states,
							zip : this.responceData.zip,
							emp_type : this.responceData.emp_type,
							education : this.responceData.education,
							speciality : this.responceData.speciality,
							weight : this.responceData.weight,
							height : this.responceData.height
					}
				});
				this.pType = 'Update';
			}
		});

	}


	onSubmit() {
		this.submitted = true; 
		this._userServ.add(this.regFields).subscribe(resdata => this.responceData = resdata, reserror => this.errorMsg = reserror, () => {

			if(this.responceData.status == '200') {
				this.toastr.success(this.responceData.data , 'Success');
				this.router.navigate(['patient_reg']);
			}else {
				this.toastr.error(this.responceData.msg, 'Failed');
			}
		});
	}


}
