import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PRegistrationComponent } from './p-registration.component';

describe('PRegistrationComponent', () => {
  let component: PRegistrationComponent;
  let fixture: ComponentFixture<PRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
