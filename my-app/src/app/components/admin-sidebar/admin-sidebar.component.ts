import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-sidebar',
  templateUrl: './admin-sidebar.component.html',
  styleUrls: ['./admin-sidebar.component.less']
})
export class AdminSidebarComponent implements OnInit {

  constructor() { }

  userData:any;

  ngOnInit() {

  	this.userData = JSON.parse( sessionStorage.getItem('user_data') );

  }

}
