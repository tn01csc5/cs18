import { AfterViewInit, Component, OnInit, Renderer } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-patient-reg-form',
  templateUrl: './patient-reg-form.component.html',
  styleUrls: ['./patient-reg-form.component.less']
})
export class PatientRegFormComponent implements AfterViewInit, OnInit {

  dtOptions: DataTables.Settings = {};

  userData: any;

  constructor(private renderer: Renderer, private router: Router) { }

  ngOnInit(): void {
    this.dtOptions = {
      ajax: { 
        url: environment.api_base_url + 'user/list',
        method: 'POST',
        data : {
        	'listType' : 'patient'
        },
        headers : {
          'Api-Token' : sessionStorage.getItem('token')
        }
      },
      columns: [{
        title: 'ID',
        data: 'id'
      }, {
        title: 'First name',
        data: 'first_name'
      }, {
        title: 'Last name',
        data: 'last_name'
      }, {
        title: 'Mobile',
        data: 'mobile'
      }, {
        title: 'Action',
        render: function (data: any, type: any, full: any) {
          return '<button class="waves-effect btn" view-person-id="'+full.id+'">View</button>';
        }
      }],

    };
  }

  ngAfterViewInit(): void {
    this.renderer.listenGlobal('document', 'click', (event) => {
      if (event.target.hasAttribute("view-person-id")) {
        this.userData = JSON.parse( sessionStorage.getItem('user_data') );
        if(this.userData.user_type == 'Doctor') {
          this.router.navigate(["/patient_details/add/" + event.target.getAttribute("view-person-id")]);
        }else {
          this.router.navigate(["/patient_reg/edit/" + event.target.getAttribute("view-person-id")]);
        }
      }
    });
  }
}