import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientRegFormComponent } from './patient-reg-form.component';

describe('PatientRegFormComponent', () => {
  let component: PatientRegFormComponent;
  let fixture: ComponentFixture<PatientRegFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientRegFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientRegFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
