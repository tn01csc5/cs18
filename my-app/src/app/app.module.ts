import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule }    from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import modules

import { DataTablesModule } from 'angular-datatables';
import { ToastrModule } from 'ngx-toastr';

// import components

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { Page404Component } from './components/page404/page404.component';
import { PRegistrationComponent } from './components/p-registration/p-registration.component';
import { LoginComponent } from './components/login/login.component';
import { AdminComponent } from './components/admin/admin.component';
import { AdminHeaderComponent } from './components/admin-header/admin-header.component';
import { AdminFooterComponent } from './components/admin-footer/admin-footer.component';
import { AdminSidebarComponent } from './components/admin-sidebar/admin-sidebar.component';
import { WebsiteComponent } from './components/website/website.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AdminUsersComponent } from './components/admin-users/admin-users.component';
import { UserRegFormComponent } from './components/user-reg-form/user-reg-form.component';


// Import Services

import { UserServiceService } from './services/user-service.service';
import { PatientRegFormComponent } from './components/patient-reg-form/patient-reg-form.component';
import { DocFormComponent } from './components/doc-form/doc-form.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AboutComponent,
    HomeComponent,
    Page404Component,
    PRegistrationComponent,
    LoginComponent,
    AdminComponent,
    AdminHeaderComponent,
    AdminFooterComponent,
    AdminSidebarComponent,
    WebsiteComponent,
    DashboardComponent,
    AdminUsersComponent,
    UserRegFormComponent,
    PatientRegFormComponent,
    DocFormComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    DataTablesModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      maxOpened:0,
      autoDismiss:false
    }) // ToastrModule added
  ],
  providers: [UserServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
