import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
	providedIn: 'root'
})



export class UserServiceService {

	constructor( private http: HttpClient ) { }

	


	login(postData : any) {
		let form = new FormData();
		form.append('email', postData.inputEmail);
		form.append('password', postData.inputPassword);
		form.append('user_type', postData.userType);
		return this.http.post(environment.api_base_url + 'user/login', form);
	}

	list() {
		let httpOptions = {
			headers: new HttpHeaders({
				'Api-Token':  sessionStorage.getItem('token')
			}) 
		};
		return this.http.get(environment.api_base_url + 'user/list', httpOptions);
	}

	add(postData : any) {
		let httpOptions = {
			headers: new HttpHeaders({
				'Api-Token':  sessionStorage.getItem('token')
			}) 
		};

		let form = new FormData();
		form.append('email', postData.email);
		form.append('first_name', postData.first_name);
		form.append('last_name', postData.last_name);
		form.append('gender', postData.gender);
		form.append('mobile', postData.contact_number);
		form.append('address1', postData.address1);
		form.append('address2', postData.address2);
		form.append('city', postData.city);
		form.append('states', postData.states);
		form.append('zip', postData.zip);
		form.append('emp_type', postData.emp_type);
		form.append('education', postData.education);
		form.append('speciality', postData.speciality);
		if(postData.weight != undefined) {
			form.append('weight', postData.weight);
			form.append('height', postData.height);
		}
		return this.http.post(environment.api_base_url + 'user/add', form, httpOptions);
	}

	edit(userId : any) {

		let httpOptions = {
			headers: new HttpHeaders({
				'Api-Token':  sessionStorage.getItem('token')
			}) 
		};

		let form = new FormData();
		return this.http.post(environment.api_base_url + 'user/edit/'+userId, form, httpOptions);
	}


	addPatient(postData : any) {
		let httpOptions = {
			headers: new HttpHeaders({
				'Api-Token':  sessionStorage.getItem('token')
			}) 
		};

		let form = new FormData();
		form.append('user_id', postData.user_id);
		form.append('symptoms', postData.symptoms);
		form.append('diagnosis', postData.diagnosis);
		form.append('pres_medicine', postData.pres_medicine);
		form.append('test_order', postData.test_order);
		form.append('sugg_specialist', postData.sugg_specialist);
		

		return this.http.post(environment.api_base_url + 'user/add_patient_medical_info', form, httpOptions);
	}

	getspecialityList() {
		let httpOptions = {
			headers: new HttpHeaders({
				'Api-Token':  sessionStorage.getItem('token')
			}) 
		};
		let form = new FormData();
		return this.http.post(environment.api_base_url + 'speciality', form, httpOptions);
	}

	getPatientHistory(user_id) {
		let httpOptions = {
			headers: new HttpHeaders({
				'Api-Token':  sessionStorage.getItem('token')
			}) 
		};
		let form = new FormData();
		form.append('user_id', user_id);
		return this.http.post(environment.api_base_url + 'user/get_patient_medical_info', form, httpOptions);
	}

}
