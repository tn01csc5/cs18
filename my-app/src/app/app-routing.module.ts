import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutComponent }  from './components/about/about.component';
import { HomeComponent }  from './components/home/home.component';
import { Page404Component }  from './components/page404/page404.component';

import { LoginComponent }  from './components/login/login.component';
import { AdminComponent }  from './components/admin/admin.component';
import { WebsiteComponent }  from './components/website/website.component';

import { DashboardComponent }  from './components/dashboard/dashboard.component';

import { AdminUsersComponent }  from './components/admin-users/admin-users.component';
import { UserRegFormComponent }  from './components/user-reg-form/user-reg-form.component';

import { PatientRegFormComponent }  from './components/patient-reg-form/patient-reg-form.component';
import { PRegistrationComponent }  from './components/p-registration/p-registration.component';

import { DocFormComponent } from './components/doc-form/doc-form.component'


const routes: Routes = [

	{ 
        path: '',
        component: WebsiteComponent, 
        children: [
          { path: '', component: HomeComponent },
          { path: 'about', component: AboutComponent },
          { path: '404', component: Page404Component },
		  { path: 'registration', component: PRegistrationComponent },
		  { path: 'login', component: LoginComponent },
        ]
    },
	{ 
        path: '',
        component: AdminComponent, 
        children: [
          { path: 'dashboard', component: DashboardComponent },
          { path: 'user_reg', component: AdminUsersComponent },
          { path: 'user_reg/add', component: UserRegFormComponent },
          { path: 'user_reg/edit/:id', component: UserRegFormComponent },
          { path: 'patient_reg', component: PatientRegFormComponent },
          { path: 'patient_reg/add', component: PRegistrationComponent },
          { path: 'patient_reg/edit/:id', component: PRegistrationComponent },
          { path: 'patient_details/add/:id', component: DocFormComponent },
        ]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
